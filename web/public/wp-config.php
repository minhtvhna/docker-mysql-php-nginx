<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'dev');

/** MySQL database password */
define('DB_PASSWORD', 'dev');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZF&KuKpf$,!7}R#;W&~iRU;)?PxBUREi#t~Ipngw9n2#/$&t.F,8Q-IRx-ol& (T');
define('SECURE_AUTH_KEY',  'A0v,0Lx0Q>}=]9&E.Jt;%d3Dfpi;(hah2Z~4BEZphccJXi!`PE)l*}9XQs|9DH|!');
define('LOGGED_IN_KEY',    'UA0Z.A3?6K&UaFQ@gZRYRgGd;BP^JL^FtlWGi;VwbiJ=)%KZ;0dThi3 <w=lv 3(');
define('NONCE_KEY',        'tM-Td_)n@?m#Tq?Tz./ S3ddTsIwsS$(~TVC|!a9-Ef9D+NU/9V5RF8&7dHBEr#T');
define('AUTH_SALT',        'w7y;cSDG[J~:TQajw)XRn)5|$Z~:=StsyNK0@IcgRA~.tIB}48CX7TS%(@9VFE?y');
define('SECURE_AUTH_SALT', 'vgSs_EC},jTCa|M<:2av?lAZR]h-!nQsd~9EJrm~v@3*lg)!EXj :YAzZe%E$VV@');
define('LOGGED_IN_SALT',   '1bHv.$l[9#+PrY{L]M>b>hh+WGx+i,U*V?2O[qH)]lHnpC k=2GG[r;HuYYMw i}');
define('NONCE_SALT',       '5+bc[Y]QOtuTB6HUnYd[3VwTq9dKb/n?YhKR +s[(78R#f_.rQ@g.XY=y0DN:bJm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

